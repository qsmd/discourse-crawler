# -----------------------------
# Makefile as Discourse crawler
# -----------------------------

OUTPUT_DIR ?= json

# AUTOMATED PART OF MAKEFILE

SHELL=/bin/bash
.PHONY: all category category-% topic topic-% clean veryclean

# keep automatically created directories; keep topic files and *.dep files
.PRECIOUS: $(OUTPUT_DIR)%/.
.PRECIOUS: $(OUTPUT_DIR)/topic-%.json $(OUTPUT_DIR)/topic-%.dep

all: category
category: category-$(CAT_ID)
topic: topic-$(TOP_ID)
clean: 
	rm $(OUTPUT_DIR)/*.dep
veryclean: 
	rm -rf $(OUTPUT_DIR)/

# complain if target is other than clean or veryclean and
# BASE_URL is not set
ifeq (,$(filter clean veryclean,$(MAKECMDGOALS)))
  ifndef BASE_URL
    $(error BASE_URL is not set)
  endif
endif

# for automatic directory creation, see
# https://ismail.badawi.io/blog/2017/03/28/automatic-directory-creation-in-make/
$(OUTPUT_DIR)/.:
	mkdir -p $@

$(OUTPUT_DIR)%/.:
	mkdir -p $@

.SECONDEXPANSION:

# list categories
# (for determining subcategories)
$(OUTPUT_DIR)/categories.json: | $$(@D)/.
	wget -O $@ $(BASE_URL)/$(@F)

# the topics of a given category will order-only depend on this target,
# dynamically defined in the corresponding category-$(CAT_ID).dep file 
category-$(CAT_ID): $(OUTPUT_DIR)/category-$(CAT_ID).json
category-$(CAT_ID): $(OUTPUT_DIR)/category-$(CAT_ID).dep

# get category
# (at the moment for archival purposes only; the JSON data contain a
# "topic_count" that could be used for building category dependencies -- see
# below -- but I don't trust the numbers right now)
$(OUTPUT_DIR)/category-%.json: | $$(@D)/.
	wget -O $@ $(BASE_URL)/c/$(@F:category-%.json=%)/show.json

# create category dependencies (= topics and subcategories)
# (Topics of subcategories are alredy included in the parent's topics.
# Unfortunately we currently download the topic pages three times: first for
# determining the total amount of pages, second for creating the list of IDs,
# and third for making them known to "make" as prerequisites to store them
# permanently. The second download could be avoided by defining a target
# "$(OUTPUT_DIR)/category-$(CAT_ID)-topics-page-%.dep" which recursively adds
# the next page as dependency. The third download could be avoided by
# creating a category-%.1.dep file that depends on categories.json and that
# creates a category-%.2.dep file that depends on the paged topic listings,
# see commit 3d8b8309. However, code readability won't increase by doing
# that, so let's not.)
$(OUTPUT_DIR)/category-$(CAT_ID).dep: $(OUTPUT_DIR)/categories.json | $$(@D)/.
	rm -f $@
	TOPIC_IDS=""; \
	PAGE=0; \
	while [ ! -z "$$(curl -sL $(BASE_URL)/c/$(CAT_ID).json?page=$${PAGE} \
	                 | jq -r ".topic_list.topics[]")" ]; \
	do \
	  TOPIC_IDS+=" "$$(curl -sL $(BASE_URL)/c/$(CAT_ID).json?page=$${PAGE} \
	                   | jq -r ".topic_list.topics[].id" \
	                   | paste --serial --delimiters=" "); \
	  let "PAGE++"; \
	done; \
	echo $${TOPIC_IDS} | tr " " "\n" \
	  | awk '{print "category-$(CAT_ID): | topic-" $$0}' >> $@; \
	seq 0 $${PAGE} | awk '{print "category-$(CAT_ID):'\
	' | $(OUTPUT_DIR)/category-$(CAT_ID)-topics-page-" $$0 ".json"}' >> $@
	cat $< | jq -r ".category_list.categories[]"\
	" | select (.id==$(CAT_ID) and .has_children) | .subcategory_ids[]" \
	  | awk '{print "category-$(CAT_ID):'\
	' | $(OUTPUT_DIR)/category-" $$0 ".json"}' >> $@

# list topics of a category page-wise
# (they also contain topics of subcategories)
$(OUTPUT_DIR)/category-$(CAT_ID)-topics-page-%.json: | $$(@D)/.
	wget -O $@ "$(BASE_URL)/c/$(CAT_ID).json"\
	"?page=$(@F:category-$(CAT_ID)-topics-page-%.json=%)"

# the posts of a given topic will order-only depend on this target, dynamically
# defined in the corresponding topic-%.dep file
topic-%: $(OUTPUT_DIR)/topic-%.dep
	: # do nothing here

# get topic
# (the JSON data list the contained posts)
$(OUTPUT_DIR)/topic-%.json: | $$(@D)/.
	wget -O $@ $(BASE_URL)/t/$(@F:topic-%.json=%.json)

# create topic dependencies (= posts)
$(OUTPUT_DIR)/topic-%.dep: $(OUTPUT_DIR)/topic-%.json | $$(@D)/.
	rm -f $@
	cat $< | jq -r ".post_stream.stream[]" \
	  | awk '{print "$(@F:topic-%.dep=topic-%):'\
	' | $(OUTPUT_DIR)/post-" $$0 ".json"}' >> $@

# get post
$(OUTPUT_DIR)/post-%.json: | $$(@D)/.
	wget -O $@ $(BASE_URL)/posts/$(@F:post-%.json=%.json)

include $(wildcard $(OUTPUT_DIR)/*.dep)


