# Discourse Crawler

This project assists in crawling content from a web server running the forum software [_Discourse_](https://www.discourse.org/).

While Discourse is machine readable via a [JSON API](https://github.com/discourse/discourse_api) (documented at [`docs.discourse.org`](https://docs.discourse.org/)), downloading e.g. all posts from a category is not possible with a single request. This `Makefile` generates the requests to download JSON files that contain publicly available data -- no API key required.

## Usage

### Quick start

Run via

```sh
make BASE_URL=https://discourse.example.org CAT_ID=10 category
```

Remake until `make` is happy. Try with `--dry-run`. Run `make clean` before switching categories.

### Longer introduction

A GNU/Linux system with GNU `make`, `wget` and friends is assumed. Also requires [`jq`](https://stedolan.github.io/jq/). Now `cd` into the directory that contains this `README.md` and run `make <options> <target>`. `<target>` may be one of the following:

| target | description | required options |
| -- | -- | -- |
| `category` | download available data about a category, contained topics and contained posts | `BASE_URL`, `CAT_ID` |
| `topic` | download available data about a topic and contained posts | `BASE_URL`, `TOP_ID` |
| `clean` | delete `*.dep` auxiliary files that do not contain forum data | (none) |
| `veryclean` | delete the entire output folder and therefore all downloaded data | (none) |

The available options are:

| option | description | default |
| -- | -- | -- |
| `BASE_URL` | protocol and domain of the Discourse instance, e.g. `https://meta.discourse.org` | (none) |
| `CAT_ID` | ID of the category to download; can be determined from the URL by browsing the category page, e.g. `https://meta.discourse.org/c/howto/10` → `10` | (none) |
| `TOP_ID` | ID of the topic to download; can be determined from the URL by browsing the topic page, e.g. `https://meta.discourse.org/t/about-the-migration-category/196969` → `196969` | (none) |
| `OUTPUT_DIR` | name of the directory below the current one where downloaded files are stored | `json` |

An example call might look like this:

```sh
make BASE_URL=https://discourse.example.org CAT_ID=10 category
```

Until you are sure, you can simulate the execution with `--dry-run`, e.g.

```sh
make --dry-run BASE_URL=https://discourse.example.org CAT_ID=10 category
```

After your first run, `make` will output `Must remake target '...'.` This is because after the first run `make` does not know about all files that need to be downloaded. Run your command again until this message disappears.

If you want to download a different category after your first, run `make clean` before.

## Known limitations

### Uploads

Images and other files that have been uploaded to Discourse and are embedded or linked from inside posts are currently not being downloaded. Arbitrary files can be identified in the HTML version of posts by a `href` starting with `<BASE_URL>/uploads/short-url/` and embedded images start with a `src` of `<BASE_URL>/uploads/default/original/1X/` (no guarantee).

### Rate limits

Discourse applies rate limits to API requests. This will lead to errors such as the following:

```
ERROR 429: Too Many Requests.
```

In this case, delete the most recent downloaded file if it is empty and try again a while later.

## Developer information

This `Makefile` digs into the forum's content by recursively visiting category → each topic → each post. In the end, not only the individual post data are downloaded, but also data from the hierarchies above. Specifically, this means that files of the following format are created:

- `category-<ID>.json`: contains some general information about a category
- `category-<ID>-topics-page-<PAGE>.json`: lists some topics inside a category (30 by default)
- `topic-<ID>.json`: contains some general information about a topic
- `post-<ID>.json`: contains the content of posts in Markdown and HTML format

Additionally, the following files are created, that are only of use during the download:

- `category-<ID>.dep`
- `topic-<ID>.dep`

They contain additional prerequisites for `make` that are dynamically generated during the first few runs. You can delete them after you have run `make` repeatedly.

## License

This project is licensed under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).

## More

Shout out to [`fx`](https://github.com/antonmedv/fx), a command-line JSON processing tool, that helped a lot while working with the API.


